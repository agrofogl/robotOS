# Makefile for compiling an ev3 project with the following folder structure:
#
#	this_folder/
# 		Makefile (this file)
#		ev3dev-c/
#		obj/
#			.o
#		libraries/
#			.a
#		include/
# 			.h
#	 	source/
#			.c
#		main (exec)

CC			= arm-linux-gnueabi-gcc
CFLAGS 		= -O3 -g -std=gnu99 -W -Wall -Wno-comment
INCLUDES 	= -Iev3dev-c/source/ev3 -Iinclude
LDFLAGS 	= -Llibraries -lrt -lm -lev3dev-c -lpthread -lbluetooth
OBJDIR 		= obj
SRCDIR		= source
SRC			= $(wildcard $(SRCDIR)/*.c)
OBJS 		= $(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRC))
EXEC 		= main

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CC) $(INCLUDES) -o $@ $^ $(LDFLAGS)

$(OBJS): $(OBJDIR)

$(OBJDIR):
	mkdir $(OBJDIR)
	
$(OBJDIR)/%.o : $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@
	
.PHONY : clean

clean:
	rm -rf $(OBJDIR)
	rm -f  $(EXEC)

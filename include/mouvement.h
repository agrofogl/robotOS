#ifndef __MOUVEMENT_H__
#define __MOUVEMENT_H__

void get_encoders_values(int * disp_left, int * disp_right);

void * thread_motor_move(void *arg);

#endif

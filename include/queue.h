#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <mqueue.h>


void init_queue (mqd_t *mq_desc, int open_flags, char * name);
void put_integer_in_mq (mqd_t mq_desc, int data);
int get_integer_from_mq (mqd_t mq_desc);

#endif

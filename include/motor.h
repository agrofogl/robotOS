#ifndef __MOTOR_H__
#define __MOTOR_H__

#include "ev3_tacho.h"

void motor_run(int port, int sens, double rapport);
void motor_stop(int port, INX_T way);
void motor_angle(int port, int angle, int sens, double pourc);
int motor_get_angle(int port);


#endif
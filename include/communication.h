#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#define MSG_ACK     0
#define MSG_START   1
#define MSG_STOP    2
#define MSG_KICK    3
#define MSG_SCORE   4
#define MSG_CUSTOM  8
#define OVER        37  


/*
open a socket to the server, with a BT communication.
When it is done, run the reception thread
*/
int init_socket();

/*
send a msg to the server, telling that we score 1-3 pts
*/
void send_score_msg(int score);

/*
send an acknowledge msg in respond to a msg received previously
status represente if the communication was good or not
*/
void send_ack_msg(char* msg,int status);

/*
send a msg to a dest. Msg to send are predefinied and hard code in the program
*/
void send_custom_msg(int, int);

void * thread_BTin(void *arg);

void * thread_BTout(void *arg);


#endif
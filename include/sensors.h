#ifndef __SENSORS_H__
#define __SENSORS_H__


extern const char *color[];

//static bool _check_pressed( uint8_t sn );

int sensors_init();

int sn_get_color_val();

int sn_color_set_mode(char* mode);

float sn_get_compass_val();

float sn_get_gyro_val();

float sn_get_sonar_val();

float sn_get_mag_val();

int sn_gyro_set_mode();

int touch_val();

#endif


#ifndef POSITION_H
#define POSITION_H

#include <pthread.h>
#include <stdbool.h>

extern bool positionth_stop;

void * update_position();

void position_stop();

void position_start(float init_x, float init_y, int heading);

void get_position(float *, float *);

int get_heading();

float get_y();

float get_x();

void set_y(float y);

void set_x(float x);

void set_heading(int heading);

float get_distance(float x1,float y1);

#endif

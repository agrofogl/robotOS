#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_sensor.h"


const char *color[] = { "?", "BLACK", "BLUE", "GREEN", "YELLOW", "RED", "WHITE", "BROWN" };
#define COLOR_COUNT  (( int )( sizeof( color ) / sizeof( color[ 0 ])))

uint8_t sn_touch;
uint8_t sn_color;
uint8_t sn_compass;
uint8_t sn_sonar;
uint8_t sn_gyro;
uint8_t sn_mag;

static bool _check_pressed( uint8_t sn )
{
  int val;
  if ( sn == SENSOR__NONE_ ) {
    return ( ev3_read_keys(( uint8_t *) &val ) && ( val & EV3_KEY_UP ));
  }
  return ( get_sensor_value( 0, sn, &val ) && ( val != 0 ));
}

/* initialisation of sensors*/
/* By Nour*/

int sensors_init ( void )
{
  if ( ev3_init() == -1 ) return ( 1 );

 
  ev3_sensor_init();

/* Sensors found */ 

  if ( ev3_search_sensor( LEGO_EV3_TOUCH, &sn_touch, 0 )) {
    printf( "TOUCH sensor is found\n" );
  }
  if ( ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0 )) {
      printf( "COLOR sensor is found\n" );
  }
  if (ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro,0)) {
      printf("GYRO found\n");
  }
  if (ev3_search_sensor(LEGO_EV3_US, &sn_sonar,0)){
      printf("SONAR found\n");
  }
  if (ev3_search_sensor(NXT_ANALOG, &sn_mag,0)){
      printf("Magnetic sensor found\n");
  }
  return(0);
}

/* call functions for sensors */
/* By Safa*/ 

/*  COLOR */

int sn_get_color_val(){
    int val;
    if ( sn_color != DESC_LIMIT) {
        get_sensor_value( 0, sn_color, &val );
        return val;
    } else {
        perror("Attempt to read uninitialized Color sensor! Aborting...\n" );
        abort();
    }
}


int sn_color_set_mode(char* mode){
    if ( sn_color != DESC_LIMIT ) {
        set_sensor_mode(sn_color, mode);
        return 0;
    } else {
        perror("Attempt to set color mode of uninitialized COLOR sensor! Aborting...\n" );
        abort();
    }
}


/*  COMPASS */

float sn_get_compass_val(){
    if ( sn_compass != DESC_LIMIT){
        float value;
        get_sensor_value0(sn_compass, &value );
        return value;
    } else {
        perror("Attempt to read uninitialized COMPASS! Aborting...\n" );
        abort();
    }
} 

/*  GYRO */

float sn_get_gyro_val(){
    if (sn_gyro != DESC_LIMIT){
        float value;
        get_sensor_value0(sn_gyro, &value );
        return value; 
    } else {
        printf("Attempt to read uninitialized GYRO! Aborting...\n");
        abort();
    }
}

int sn_gyro_set_mode(char* mode){
    if ( sn_gyro != DESC_LIMIT ) {
        set_sensor_mode(sn_gyro, mode);
        return 0;
    } else {
        perror("Attempt to set gyro mode of uninitialized gyro sensor! Aborting...\n" );
        abort();
    }
}
/* SONAR */

float sn_get_sonar_val(){
    if (sn_sonar != DESC_LIMIT){
        float value;
        get_sensor_value0(sn_sonar, &value );
        return value;
    } else {
        printf("Attempt to read uninitialized SONAR! Aborting...\n");
        abort();
    }
}

/*  MAG */
float sn_get_mag_val(){
    if (sn_mag != DESC_LIMIT){
        float value;
        get_sensor_value0(sn_mag, &value );
        return value;
    } else {
        printf("Attempt to read uninitialized MAG! Aborting...i\n");
        abort();
    }
}

/* TOUCH */
int touch_val(){
    if (_check_pressed( sn_touch )){
        return 1;
    }
    else{
        return 0;
    }
}


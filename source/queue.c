#include <mqueue.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define MQ_SIZE 10
#define PMODE 0666


/* to open the message queue */
/* must start with a "/" */
void init_queue (mqd_t *mq_desc, int open_flags, char * name) {
  struct mq_attr attr;
  
  // fill attributes of the mq
  attr.mq_maxmsg = MQ_SIZE;
  attr.mq_msgsize = sizeof (int);
  attr.mq_flags = 0;

  // open the mq
  *mq_desc = mq_open (name, open_flags, PMODE, &attr);
  if (*mq_desc == (mqd_t)-1) {
    perror("Mq opening failed46513");
    exit(-1);
  }
}


/* to add an integer to the message queue */
void put_integer_in_mq (mqd_t mq_desc, int data) {
  //sends message
  int status = mq_send (mq_desc, (char *) &data, sizeof (int), 1);
  if (status == -1)
    perror ("mq_send failure");
}


/* to get an integer from message queue */
int get_integer_from_mq (mqd_t mq_desc) {

  int data=0;
  //receive an int from mq
  ssize_t num_bytes_received = mq_receive (mq_desc, (char *) &data, sizeof (int), NULL);
  if (num_bytes_received == -1)
    perror ("mq_receive failure");
  return (data);
}

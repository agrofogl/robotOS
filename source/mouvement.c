#include <stdio.h>
#include <mqueue.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "queue.h"
#include "motor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__
#include <windows.h>
// UNIX //////////////////////////////////////////
#else
#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )
//////////////////////////////////////////////////
#endif

// CARACT PHYSIQUE ///////////////////////////////
#define PORT_MOTOR_L    65   
#define PORT_MOTOR_R    66

int max_speed;
//////////////////////////////////////////////////
#define OVER 37  

void get_encoders_values(int * disp_left, int * disp_right){
    get_tacho_position(PORT_MOTOR_L, disp_left);
    get_tacho_position(PORT_MOTOR_R, disp_right);
}

 
static mqd_t mqueue_order_mot;

static void move_forward() {
    motor_run(PORT_MOTOR_L, 1, 0.4);
    motor_run(PORT_MOTOR_R, 1, 0.4); 
}

static void move_backward() {
    motor_run(PORT_MOTOR_L, -1, 0.4);
    motor_run(PORT_MOTOR_R, -1, 0.4); 
}
   /* tourne les 2 moteurs a la mm vitesse avec le meme tps de monter et de descente (integration numerique ?) */

static void rotate_clockwise() {
    motor_run(PORT_MOTOR_L, 1, 0.05);
    motor_run(PORT_MOTOR_R, -1, 0.05); 
}

static void rotate_anti_clockwise() {
    motor_run(PORT_MOTOR_L, -1, 0.05);
    motor_run(PORT_MOTOR_R, 1, 0.05); 
}
    /*tourne les moteurs en sens opposés pour que le robot tourne sur son axe (vitesse de rotation a tester en fonction 
    des perfs du moteurs -> mesurer le tps)*/

static void sig_stop () {
    motor_stop(PORT_MOTOR_L, TACHO_COAST);
    motor_stop(PORT_MOTOR_R, TACHO_COAST);
}

static void init_motor_direction() {
    char s[256];
    uint8_t sn;
    while ( ev3_tacho_init() < 1 ) {
        sleep(1);
        printf("waiting for tacho_init...\n");
    }

    printf( "*** ( EV3 ) Hello! ***\n" );

    printf( "Found tacho motors:\n" );
    for ( int i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
            printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
            printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
        }
    }
    //init the max speed
    if (ev3_search_tacho_plugged_in(PORT_MOTOR_L, 0, &sn, 0)) {
        get_tacho_max_speed( sn, &max_speed );
        max_speed = (int) max_speed * 2/3; // on se base sur le moteur gauche
    }

    //init the queue
    init_queue (&mqueue_order_mot, O_RDONLY, "/motorQ");
    //associate signal
    signal(SIGUSR1, sig_stop);
   return ;
}

//le thread qui gere le moteur
void * thread_motor_move(void *arg)
{
    (void) arg;
    int foo;
    init_motor_direction();
    sleep(1);
    while (1) {
        foo = get_integer_from_mq (mqueue_order_mot);
        switch((int) foo/10) {
            case 0 :
                motor_stop(PORT_MOTOR_L, TACHO_COAST);
                motor_stop(PORT_MOTOR_R, TACHO_COAST);
                sleep(3);
                continue;
            case 1 :
                (foo%10) ? move_forward() : move_backward();
                continue;
            case 2 : 
                (foo%10) ? rotate_clockwise() : rotate_anti_clockwise();
                continue;
            case OVER :
                motor_stop(PORT_MOTOR_L, TACHO_COAST);
                motor_stop(PORT_MOTOR_R, TACHO_COAST);
                exit(0);
            default :
                continue;
        }
    }
}

#include <time.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "navigation.h"
#include "ev3.h"
#include "ev3_tacho.h"
#include "queue.h"
#include "motor.h"
#include "mouvement.h"
#include "sensors.h"
#include "position.h"

#define WHEEL_DIAMETER 5.6
#define TICKS_PER_REVOLUTION 360

extern mqd_t mqueue_order_mot;
//M_PI define in math.h
const float ENCODER_SCALE_FACTOR = M_PI * WHEEL_DIAMETER / TICKS_PER_REVOLUTION;

float POS_X = 0.0; /** position initiale */

float POS_Y = 0.0; /** position initiale */

int HEADING = 0; 

pthread_t position_tid = 0;
pthread_mutex_t position_mutex;

bool positionth_stop = false;

void * update_position(){
  int prev_ticks_left;
  int ticks_left;
  int prev_ticks_right;
  int ticks_right;
  int angle = 0;
  int prev_angle = 0;
  float displacement;

  prev_ticks_left=motor_get_angle(65);
  prev_ticks_right=motor_get_angle(66);
  prev_angle = sn_get_gyro_val();

  while(!positionth_stop){
        
          ticks_left=motor_get_angle(65);
          ticks_right=motor_get_angle(66);

        
        displacement = (ticks_left - prev_ticks_left+ticks_right - prev_ticks_right) * ENCODER_SCALE_FACTOR / 2.0;
        
        angle = sn_get_gyro_val();

        pthread_mutex_lock(&position_mutex);
        HEADING += angle-prev_angle;
        
        POS_X += displacement*cos(HEADING*M_PI/180.0);
        POS_Y += displacement*sin(HEADING*M_PI/180.0);
        pthread_mutex_unlock(&position_mutex);
        
        prev_angle = angle;

        //printf("POS: X: %.2f \t Y: %.2f \t HEADING: %.02d ticks_left: %d ticks_right: %d prev_ticks_left: %d prev_ticks_right: %d\n",POS_X,POS_Y, HEADING, ticks_left, prev_ticks_left, ticks_right, prev_ticks_right); 
        
        prev_ticks_left = ticks_left;
        prev_ticks_right = ticks_right;
  }
}

int check_position(){
  float x = get_x();
  float y = get_y();
  if ((x<2) || (x>98) || (y<2) || (y>58)){
    put_integer_in_mq(mqueue_order_mot, 0);
    go_to(0,0);
    return 0;
  }
  else{
    return 1;
  }
}

void position_stop(){
    positionth_stop = true;
    pthread_join(position_tid, NULL);
}

void position_start(float init_x, float init_y, int heading){
    if (position_tid != 0)
        return;

    pthread_mutex_init(&position_mutex, NULL);
    pthread_mutex_lock(&position_mutex);

    HEADING = heading;
    POS_X = init_x;
    POS_Y = init_y;
    pthread_mutex_unlock(&position_mutex);
    printf("Creating position tracking thread... ");
    pthread_create(&position_tid, NULL, update_position, NULL);
    printf("Position tracking thread creadted ");
}

void get_position(float * x, float *y){
    pthread_mutex_lock(&position_mutex);
    *x = POS_X;
    *y = POS_Y;
    pthread_mutex_unlock(&position_mutex);
}

float get_x(){
    float x;
    pthread_mutex_lock(&position_mutex);
    x=POS_X;
    pthread_mutex_unlock(&position_mutex);
    return x;
}

void set_x(float x){
    pthread_mutex_lock(&position_mutex);
    POS_X=x;
    pthread_mutex_unlock(&position_mutex);
}

float get_y(){
    float y;
    pthread_mutex_lock(&position_mutex);
    y=POS_Y;
    pthread_mutex_unlock(&position_mutex);
    return y;
}

void set_y(float y){
    pthread_mutex_lock(&position_mutex);
    POS_Y=y;
    pthread_mutex_unlock(&position_mutex);
}

int get_heading(){
    int heading;
    pthread_mutex_lock(&position_mutex);
    heading = HEADING;
    pthread_mutex_unlock(&position_mutex);
    printf("get_heading : %d\n", heading);
    return heading;
}

void set_heading(int heading){
    pthread_mutex_lock(&position_mutex);
    HEADING=heading;
    pthread_mutex_unlock(&position_mutex);
}

float get_distance(float x1,float y1){
    float x0;
    float y0;
    float dist;
    pthread_mutex_lock(&position_mutex);
    x0=POS_X;
    y0=POS_Y;
    pthread_mutex_unlock(&position_mutex);
    dist = sqrt( pow((x0-x1),2) + pow((y0-y1),2));
    return dist;
}


/*
float distance_to_wall(){
    int heading = get_heading();
    float d;

    if (heading<=90 && heading >=45){
         d=60/cos((M_PI/2)-heading*M_PI/180.0);
            

    } else if (heading<45 && heading>0){
         d=100/cos(heading*M_PI/180.0);
	

    } else if (heading<-45 && heading>-90){
         d=60/cos((M_PI/2)-heading*M_PI/180.0);
            
     } else if (heading<0 && heading>-45){
         d=100/cos(heading*M_PI/180.0);
	
    }
       else if(heading=0)
	{ 100;
	}
	printf("d=%f",d);
	return d;
}
*/

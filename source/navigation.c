#include <stdio.h>
#include <stdlib.h>
#include "mouvement.h"
#include "sensors.h"
#include <time.h>
#include <math.h>
#include <unistd.h>
#include "position.h"
#include "navigation.h"
#include "queue.h"
#include <time.h>
#include "catapult.h"



#define ANGULAR_VELOCITY 90 //(°/sec)
#define VELOCITY 8.83 // cm/s

extern mqd_t mqueue_order_mot;
extern mqd_t mqueue_cata;
int h_erreur=0;

void go_heading(int h)
{       int h0 = get_heading() % 360;
	int heading;
	int q=0;
	if (h>h0)
	{
		q=20;
	}
	else if (h<h0)
	{
		q=21;
	}
	printf("q=%d \n",q);
	put_integer_in_mq(mqueue_order_mot, q);
  	int Test = 0;
  	while (Test!= 1){
     		heading=get_heading();
		if (heading<h+2 && heading>h-2) 
   		{ 
      	   		Test = 1;
      	   		put_integer_in_mq(mqueue_order_mot, 0);
   		}
    	}
}


void go_to(float x, float y){
    //M_PI define in math.h
    put_integer_in_mq(mqueue_order_mot, 0);
    int heading = get_heading() ;
    float x1, y1, dist;
    //int rot_angle = ((int)roundf(theta))+heading;
    printf("avant le get position\n");
    get_position(&x1, &y1);
	if (x>x1)
	{
	go_heading(0);	
	}
     else if (x<x1) 
	{go_heading(180);
	}
    int test=0;
    float distance;
    put_integer_in_mq(mqueue_order_mot, 11);
    while (test != 1){
      get_position(&x1, &y1);
      distance=fabs(x-x1);
      printf("dans le while avance x=%f y=%f\n",get_x(),get_y());
      printf("get_dist= %f \n",fabs(x-x1) );
      if (fabs(x-x1)<3 ) 
   	 { 
         test = 1;
         put_integer_in_mq(mqueue_order_mot, 0);
   	 }
      if (x1<0) 
   	 { 
         test = 1;
         put_integer_in_mq(mqueue_order_mot, 0);
	 
	 go_heading(0);
   	 }
       
    }
    sleep(1);
    get_position(&x1, &y1);

    if (y>y1)
	{
	go_heading(90);	
	}
     else if (y<y1)
	{go_heading(-90);
	}
    test=0;
    put_integer_in_mq(mqueue_order_mot, 11);
    while (test != 1){
      get_position(&x1, &y1);
      distance=fabs(y-y1);
      printf("dans le while avance x=%f y=%f\n",get_x(),get_y());
      printf("get_dist= %f \n",fabs(y-y1) );
      if (fabs(y-y1)<3 ) 
   	 { 
         test = 1;
         put_integer_in_mq(mqueue_order_mot, 0);
   	 }
    }
    printf("apres le while\n");
    
    sleep(1);
/*
    int theta;
	if (x-x1==0 && y1<y) 
	{ theta=90;}
	else if (x-x1==0 && y1>y) 
	{ theta=-90;}
	else 
{	theta= (int)roundf(atan2(y-y1,x-x1)*180.0/M_PI);
	}
    printf("apres le get_position\n");
    dist = sqrt( pow((x-x1),2) + pow((y-y1),2));
    put_integer_in_mq(mqueue_order_mot, 0);
    go_heading(theta);
    sleep(2);
    printf("avant le while\n");
    put_integer_in_mq(mqueue_order_mot, 11);
    int test=0;
    float distance;
    while (test != 1){
      distance=get_distance(x,y);
      printf("dans le while avance x=%f y=%f\n",get_x(),get_y());
      printf("get_dist= %f \n", distance);
      if (distance<3 && distance>-3) 
    { 
         test = 1;
         put_integer_in_mq(mqueue_order_mot, 0);
    }
	printf("test : %d \n",test);
    }    
    printf("apres le while\n");
    
    sleep(1);
*/
}



int find_ball1(int deb, int fin)
{ float a_i,dist,trip_time,a,x;
  int Test=2;
  put_integer_in_mq(mqueue_cata, 4);
  sleep(1);
  a_i = get_heading();
  x=sn_get_sonar_val();
  go_heading(deb);
  put_integer_in_mq(mqueue_order_mot, 21);
  while (Test ==2)
  { 
	a=get_heading();
	printf("wall dist = %f\n",sn_get_sonar_val());
	if (sn_get_sonar_val()<450 && sn_get_sonar_val()!=326 ) 
  	{	
		put_integer_in_mq(mqueue_order_mot, 0);
		go_heading(a-20);
  	 	Test=is_ball1();
  	}
	if (Test==-999)
	{	printf("Actually it wasn't a Ball\n");
                find_ball1(a,fin);
			}
	if (abs(a)>=abs(fin)+5) 
  	{
  	 	Test=-1000;
  	}
  }
  if (Test!=0 && Test>-400)
  {
  	put_integer_in_mq(mqueue_order_mot, 0);
  	go_heading(Test);
        put_integer_in_mq(mqueue_order_mot, 11);
        int test2=1;
        while (test2)
                {
			if (sn_get_sonar_val()<80)
				{ test2=0;
				 put_integer_in_mq(mqueue_order_mot, 0);
        			}
		}
    Test=1;
  }
  else{
    Test=0;
  }
  put_integer_in_mq(mqueue_order_mot, 0);
  printf("c'est fini");
  return Test;
}

int find_ball(){
    int heading_ball=-999;
    int heading;
    int Test=1;
    int Test1=1;
    float delta;
    float value1;
    put_integer_in_mq(mqueue_cata, 4);
    sleep(2);
    go_heading(90);
    sleep(2);
    float value0=sn_get_sonar_val();
    printf("I will start looking for a ball");
    put_integer_in_mq(mqueue_order_mot, 21);
    while(1){
        value1=sn_get_sonar_val();
        delta = value1-value0;
	heading=get_heading();
	if(heading>55 && heading<65)
	{Test1=0;
	}
	if(heading>-65 && heading<-55)
	{Test1=0;
	}
	if (heading>100 )
	{
		Test=0;
	}
	if (heading<-100 )
	{
		Test=0;
	}
	
	printf("delta %f \n",delta);
	value0=value1;
	
        if (abs(delta)>50 && Test1==1){
	    put_integer_in_mq(mqueue_order_mot, 0);
	    printf("I may have found a ball !! "); 
	    sleep(2);
	    go_heading(heading-15);
	    printf("delta =%f\n",delta);
            heading_ball=is_ball();
        }
        value0=value1;
	sleep(2);
	printf("heading_ball=%d\n",heading_ball);
	sleep(2);
     	if (heading_ball>=-100 )
	{	Test=0;
	}  
    }
    printf("I am out!!!!!!!!");
    put_integer_in_mq(mqueue_order_mot, 0);
    if (heading_ball>=-100 )
	{
    		go_heading(heading_ball);   
                put_integer_in_mq(mqueue_order_mot, 11);
                int test2=1;
                while (test2)
                {
			if (sn_get_sonar_val()<65)
				{ test2=0;
				 put_integer_in_mq(mqueue_order_mot, 0);
        			}
  }
  }
    if (Test>-100 && Test< 100)
	{return 1;}
    else {return 0;}
}
int is_ball1(){
    int Test=0;
    float heading_ball=0;
    int heading0;
    int heading1;
    int test_delta_ang=0;
    float value0;
    float delta;
    float value1;
    float value01;
    float value11;
    heading0=get_heading();
    value0=sn_get_sonar_val();
    printf("START IS ball \n ");
    sleep(1);
    put_integer_in_mq(mqueue_order_mot, 20);
    while (Test==0){
        heading1=get_heading();
        value1=sn_get_sonar_val();
        
        
        if (value1<450){
	    heading_ball+=heading1;
	    printf("headin_ball= %f\n",heading_ball);
	    
            test_delta_ang+=1;
        }
        
        value0=value1;
        
        if (abs(heading1-heading0)>=40){
            put_integer_in_mq(mqueue_order_mot, 0);
            Test=1;
        }
    }
    value01=sn_get_sonar_val();
    int Test2=0;
    heading0=get_heading();
    printf(" 3ieme verif \n");
    sleep(1);
    put_integer_in_mq(mqueue_order_mot, 21);
    while(Test2==0){
	heading1=get_heading();
        value11=sn_get_sonar_val();
        if (value11<450){
	    heading_ball+=heading1;
	    test_delta_ang+=1;
        }
        value01=value11;
        if (abs(heading1-heading0)>=40){
            put_integer_in_mq(mqueue_order_mot, 0);
            Test2=1;
        }
    }
    if (test_delta_ang != 0) 
	{
	float ret = heading_ball/test_delta_ang;
	printf("RET %f \n ", ret);
	return  ret;
	}
    else 
	{return -999;
	}
}

int is_ball(){
    int Test=0;
    float heading_ball=0;
    int heading0;
    int heading1;
    int test_delta_ang=0;
    float value0;
    float delta;
    float value1;
    float value01;
    float value11;
    heading0=get_heading();
    value0=sn_get_sonar_val();
    printf("START\n ");
    sleep(2);
    put_integer_in_mq(mqueue_order_mot, 20);
    while (Test==0){
        heading1=get_heading();
        value1=sn_get_sonar_val();
        delta = value1-value0;
        printf("Delta1 %f \n", delta);
        if (abs(delta)>30){
	    heading_ball+=heading1;
	    printf("headin_ball= %f\n",heading_ball);
	    printf("delta =%f\n",delta);
            test_delta_ang+=1;
        }
        
        value0=value1;
        
        if (abs(heading1-heading0)>=40){
            put_integer_in_mq(mqueue_order_mot, 0);
            Test=1;
        }
    }
    value01=sn_get_sonar_val();
    int Test2=0;
    heading0=get_heading();
    printf("BECH NDOUR 3");
    sleep(2);
    put_integer_in_mq(mqueue_order_mot, 21);
    while(Test2==0){
	heading1=get_heading();
	printf("Delta2 %f \n", delta);
        value11=sn_get_sonar_val();
        delta = value11-value01;
        if (abs(delta)>30){
	    heading_ball+=heading1;
	    printf("delta =%f\n",delta);
            test_delta_ang+=1;
        }
        value01=value11;
        
        if (abs(heading1-heading0)>=40){
            put_integer_in_mq(mqueue_order_mot, 0);
            Test2=1;
        }
    }
    if (test_delta_ang != 0)
	{
	float ret = heading_ball/test_delta_ang;
	printf("RET %f \n ", ret);
	return  ret;
	}
    else 
	{return -9999;
	}
}
void calibrate()
{
	float dist;
	put_integer_in_mq(mqueue_order_mot, 0);
	go_heading(180);
	put_integer_in_mq(mqueue_order_mot, 10);
        int test1=1;
        while (test1)
                {       
			if (touch_val())
				{ test1=0;
				sleep(1);
				 put_integer_in_mq(mqueue_order_mot, 0);
        			}
		}

	set_heading(180);
	set_x(60);
        printf("heading:%d\n", get_heading());

        printf("x:%f\n", get_x());
	go_heading(-90);
	put_integer_in_mq(mqueue_order_mot, 10);
        int test2=1;
        while (test2)
                {       
			if (touch_val())
				{ test2=0;
				sleep(1);
				 put_integer_in_mq(mqueue_order_mot, 0);
        			}
		}

	set_heading(-90);
	set_y(40);
	printf("y:%f\n", get_y());
	printf("heading:%f\n", get_heading());

}
void go_mark()
{
	put_integer_in_mq(mqueue_cata, 3);
	go_to(20,0);
	go_heading(0);
	sleep(1);
	put_integer_in_mq(mqueue_cata, 2);
	sleep(1);
	put_integer_in_mq(mqueue_cata, 5);
}
void navigation ()
{	int i=0;
	int Test=0;
	
	while(1)
	{	Test=find_ball1(90,-90);
		if (Test)
		{	sleep(1);
			go_mark();
			sleep(1);
			//put_integer_in_mq(mqueue_cata, 2);
		}
		else
		{	
			go_to(20,0);
			Test=find_ball1(0,-360);
                        if (Test)
		{	  sleep(1);
			  go_mark();
			  sleep(1);
		}
                 	else
                        {
                           go_to(70,0);
			   Test=find_ball1(-90,-270);
                           if (Test)
		           {	  
                              sleep(1);
			      go_mark();
			      sleep(1);
		           }   
			   else
                           {
                             go_to(60,40);
                             Test=find_ball1(-90,-180);
                             if (Test)
		             {	  
                              sleep(1);
			      go_mark();
			      sleep(1);
		              }
                              else
                              {
                                go_to(60,-40);
                             Test=find_ball1(-180,-270);
                             if (Test)
		             {	  
                              sleep(1);
			      go_mark();
			      sleep(1);
		              }
			     else 
			      {go_to(20,0);
				}
                           }
                         }  
                       }

		}
            
	    calibrate();
            go_to(0,0);
            go_heading(0);
          }
}

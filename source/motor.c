#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "queue.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__
#include <windows.h>
// UNIX //////////////////////////////////////////
#else
#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )
//////////////////////////////////////////////////
#endif

extern int max_speed;

//run indefinitly
void motor_run(int port, int sens, double rapport) {
    uint8_t sn;
    
    if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
        int max = max_speed * ((sens < 0)? -1 : 1) * rapport;
        printf( "  speed = %d\n", max );
        set_tacho_speed_sp( sn, max);
        set_tacho_ramp_up_sp( sn, 100);
        set_tacho_command_inx( sn, TACHO_RUN_FOREVER );
        return;
    } else perror( "LEGO_EV3_M_MOTOR is NOT found\n" );
    exit(-1);
}


//way = TACHO_COAST, TACHO_BREAK, TACHO_HOLD
void motor_stop(int port, INX_T way) {
    uint8_t sn;    
    if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
        set_tacho_stop_action_inx( sn, way );
        set_tacho_command_inx( sn, TACHO_STOP );
        return;
    } else perror( "LEGO_EV3_M_MOTOR is NOT found\n" );
    exit(-1);
}

int motor_get_angle(int port) {
    uint8_t sn;  
    if ( ev3_search_tacho_plugged_in(port,0, &sn, 0 )) {
        int foo;
        get_tacho_position(sn, &foo);
        return foo;
    } else perror( "LEGO_EV3_M_MOTOR is NOT found\n" );
    exit(-1);
}

//TODO : gerer le sens
int motor_angle(int port, int angle, int sens, double pourc) {
    uint8_t sn;
    if (ev3_search_tacho_plugged_in(port, 0, &sn, 0)) {
        printf( "run to angle %d\n", angle);
        set_tacho_ramp_up_sp( sn, 0);
        set_tacho_ramp_down_sp( sn, 0);
        set_tacho_speed_sp( sn,  max_speed * ((sens < 0)? -1 : 1) * pourc );
        set_tacho_stop_action_inx( sn, TACHO_HOLD );
        set_tacho_position_sp( sn, angle);
        set_tacho_command_inx( sn, TACHO_RUN_TO_ABS_POS );
        return angle;
    } else {
        perror( "LEGO_EV3_M_MOTOR is NOT found\n" );
        exit(-1);
    }
}
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <mqueue.h>
#include "mouvement.h"
#include "queue.h"
#include "catapult.h"
#include "navigation.h"
#include "position.h"
#include "sensors.h"
#include "communication.h"

//moteurs pour le mouvement
mqd_t mqueue_order_mot;
pthread_t threadMot;
pthread_t threadPos;
//moteur pour le lancer
mqd_t mqueue_cata;
mqd_t mqueue_cata_state;
pthread_t threadCata;
//pour la communication BT
pthread_t threadBTin;
pthread_t threadBTout;
mqd_t mqueue_BTin;
mqd_t mqueue_BTout;


void stop_all() {
  pthread_kill(threadMot, SIGUSR1);  
  pthread_kill(threadCata, SIGUSR1);
  put_integer_in_mq(mqueue_BTout, OVER);
  put_integer_in_mq(mqueue_BTin, OVER);
  put_integer_in_mq(mqueue_cata, OVER);
  put_integer_in_mq(mqueue_order_mot, OVER);
  printf("My battery is low and it's getting dark\n");
  return;
}


int main(void) {
    
    //motor direction
    printf("init motor \n");
    init_queue (&mqueue_order_mot, O_CREAT | O_WRONLY, "/motorQ");
    sleep(1);
    if (pthread_create(&threadMot, NULL, thread_motor_move, NULL)) { perror("error thread moteur"); }
    sleep(1);

    //motor catapult
    printf("init Catapult out\n");
    init_queue (&mqueue_cata, O_CREAT | O_WRONLY, "/cataQ");
    sleep(1);
    if (pthread_create(&threadCata, NULL, thread_motor_cata, NULL)) { perror("error thread cata"); }
    sleep(1);
    printf("init Catapult in\n");
    init_queue (&mqueue_cata_state, O_RDONLY, "/cataStateQ");
    // init the arm to shoot 2 balls
    put_integer_in_mq(mqueue_cata, 3);
    put_integer_in_mq(mqueue_order_mot, 0);
    //sensors
    sensors_init();
    sleep(1);
    position_start(0,0,0); 

    /*
    // BT 
    printf("init BTout\n");
    init_queue (&mqueue_BTout, O_CREAT | O_WRONLY, "/btOutQ");
    sleep(1);
    printf("init sock\n");
    init_socket();
    sleep(1);
    printf("init BTin\n");
    init_queue (&mqueue_BTin, O_RDONLY | O_NONBLOCK, "/btInQ");
    sleep(1);
    printf("init thread\n");
    if (pthread_create(&threadBTin, NULL, thread_BTin, NULL)) { perror("error thread BT in"); }
    if (pthread_create(&threadBTout, NULL, thread_BTout, NULL)) { perror("error thread BT out"); }
    sleep(2);
    
    //Attente du signal START
    int i = 0;
    while(get_integer_from_mq(mqueue_BTin) != MSG_START && i<30) {
      printf("dans la boucle\n");
      i++;
      sleep(1);
    }
    */
    sleep(10);
    //ici on att le msg
    //throw far
    put_integer_in_mq(mqueue_cata, 2);
    //put_integer_in_mq(mqueue_BTout, 43);
    sleep(3);
    //get special ball
    put_integer_in_mq(mqueue_cata, 6);
    sleep(5);
    //throw far special
    put_integer_in_mq(mqueue_cata, 7);
    //put_integer_in_mq(mqueue_BTout, 43);
    sleep(3);
    //reset
    put_integer_in_mq(mqueue_cata, 4);
    sleep(5);


    //navigation contain the main loop
    navigation();

    //Fin des differents services
    stop_all();
    pthread_join(threadBTin, NULL);
    pthread_join(threadBTout, NULL);
    pthread_join(threadMot, NULL);
    ev3_uninit();
    return 0;
}


// Test go_to
/*
int main(void) {
    init_queue (&mqueue_order, O_CREAT | O_WRONLY, "/motorQ");
    sleep(1);
    if (pthread_create(&threadMot, NULL, thread_motor_move, NULL)) { perror("error thread"); }
    sleep(1);
    sensors_init();
    sleep(1);
    position_start(0,0,0);
    sleep(10);
    go_to(100,100);
    sleep(2);
    pthread_kill(threadMot, SIGUSR1);
    pthread_join(threadMot, NULL);
    return 0;
}



//Test des Capteurs
/*
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "sensors.h"

int main ( void )
{
  sensors_init();
  sn_color_set_mode("COL-COLOR");
  sn_gyro_set_mode("GYRO-ANG");
  while(1)
  {
    printf("color %d" , sn_get_color_val());
    printf("compass %f" , sn_get_compass_val());
    printf("sonar %f" , sn_get_sonar_val());
    printf("%f" , sn_get_gyro_val());
    printf("%f" , sn_get_mag_val());
  }
}
*/

//Test pour le moteur
/*
int main(void) {
    pthread_t threadMot;
    mqd_t mqueue_order;
    init_queue (&mqueue_order, O_CREAT | O_WRONLY, "/motorQ");
    sleep(1);
    if (pthread_create(&threadMot, NULL, thread_motor_move, NULL)) { perror("error thread"); }
    sleep(1);
    put_integer_in_mq(mqueue_order, 10);
    sleep(3);
    put_integer_in_mq(mqueue_order, 0);
    sleep(2);
    put_integer_in_mq(mqueue_order, 11);
    sleep(3);
    put_integer_in_mq(mqueue_order, 0);
    sleep(2);
    put_integer_in_mq(mqueue_order, 20);
    sleep(3);
    put_integer_in_mq(mqueue_order, 0);
    sleep(2);
    put_integer_in_mq(mqueue_order, 21);
    sleep(3);
    pthread_kill(threadMot, SIGUSR1);
    pthread_join(threadMot, NULL);
    return 0;
}
*/


//test pour la cata
/*
int main(void) {
    pthread_t threadMotCata;
    mqd_t mqueue_cata;
    init_queue (&mqueue_cata, O_CREAT | O_WRONLY, "/cataQ");
    sleep(1);
    if (pthread_create(&threadMotCata, NULL, thread_motor_cata, NULL)) { perror("error thread"); }


    //init
    put_integer_in_mq(mqueue_cata, 3);
    //ici on att le msg
    sleep(5);
    int i = 0;
      printf("boucle\n");
    //throw far
    put_integer_in_mq(mqueue_cata, 2);
    sleep(3);
    //get special ball
    put_integer_in_mq(mqueue_cata, 6);
    sleep(4);
    //throw far special
    put_integer_in_mq(mqueue_cata, 7);
    sleep(6);

    //reset
    put_integer_in_mq(mqueue_cata, 5);
    sleep(10);
    i++;
    
    printf("OVER\n");
    put_integer_in_mq(mqueue_cata, OVER);


    printf("killing\n");
    pthread_kill(threadMotCata, SIGUSR1);
    pthread_join(threadMotCata, NULL);
    printf("end\n");
    return 0;
}
*/
#include <stdio.h>
#include <mqueue.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "queue.h"
#include "motor.h"
// WIN32 /////////////////////////////////////////
#ifdef __WIN32__
#include <windows.h>
// UNIX //////////////////////////////////////////
#else
#include <unistd.h>
#define Sleep( msec ) usleep(( msec ) * 1000 )
//////////////////////////////////////////////////
#endif

// CARACT PHYSIQUE ///////////////////////////////
#define PORT_MOTOR_CATA 68
//angle dans lequel le bras se place quand le robot se deplace pour recuperer une balle.
#define ANGLEWAITING    -50
//angle de catapultage loin
#define ANGLEFAR        -328
#define ANGLEFAR2       -528
//angle de catapultage proche
#define ANGLENEAR       -335
//angle pour bien mettre la balle dans le bras
#define ANGLECATCH      -280
#define ANGLECATCH2     -480
//angle de voyage avec une balle
#define ANGLETRAVEL     -250

extern int max_speed;
//////////////////////////////////////////////////
#define OVER 37   
static mqd_t mqueue_cata;
static mqd_t mqueue_cata_state;
static int angle_init;
static uint8_t sn;
//on suppose que le bras est dans la position travel (= with a ball)
//puis on se replace sur l'angle d'attente
static void launch_far() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLEFAR, 1, 0.88);
    sleep(5);
}

static void launch_far_special() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLEFAR2, 1, 0.88);
    sleep(5);
}

//on suppose que le bras est dans la position travel (= with a ball)
//puis on replace sur l'angle d'attente
static void launch_close() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLENEAR, 1, 0.5);
    sleep(3);
}

//suppose that the arm is in the ANGLEWAITING position
static void get_a_ball() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLECATCH, 1, 0.2);
    //sleep(5);
    //motor_angle(PORT_MOTOR_CATA, angle_init - ANGLETRAVEL, 1, 0.2);
    char buf[256];
    do {
        get_tacho_state(sn, buf, sizeof(buf));
        if(strcmp(buf, "running overloaded stalled") == 0) {
            motor_run(PORT_MOTOR_CATA, -1, 0.1);
            usleep(100);
            motor_stop(PORT_MOTOR_CATA, TACHO_COAST);
            put_integer_in_mq(mqueue_cata_state, 1);
            return;
        }
        usleep(100);
    } while(strcmp(buf, "holding") != 0);
    put_integer_in_mq(mqueue_cata_state, 0);
}

static void get_a_ball_special() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLECATCH2, 1, 0.2);
}

static void makeZero() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLEWAITING, 1, 0.1);
    sleep(1);
}

static void reset_arm() {
    motor_angle(PORT_MOTOR_CATA, angle_init - ANGLEWAITING, 1, 0.1);
    sleep(2);
}


// traitement des signaux
static void sig_stop () {
    motor_run(PORT_MOTOR_CATA, 1, 0.1);
    sleep(1);
    motor_stop(PORT_MOTOR_CATA, TACHO_COAST);
}

static void init_motor_cata() {
    char s[256];
    while ( ev3_tacho_init() < 1 ) {
        sleep(1);
        printf("waiting for tacho_init...\n");
    }

    printf( "*** ( EV3 ) Hello! ***\n" );

    printf( "Found tacho motors:\n" );
    for ( int i = 0; i < DESC_LIMIT; i++ ) {
        if ( ev3_tacho[ i ].type_inx != TACHO_TYPE__NONE_ ) {
            printf( "  type = %s\n", ev3_tacho_type( ev3_tacho[ i ].type_inx ));
            printf( "  port = %s\n", ev3_tacho_port_name( i, s ));
            printf("  port = %d %d\n", ev3_tacho_desc_port(i), ev3_tacho_desc_extport(i));
        }
    }
    //init the max speed
    if (ev3_search_tacho_plugged_in(PORT_MOTOR_CATA, 0, &sn, 0)) {
        get_tacho_max_speed( sn, &max_speed );
        max_speed = (int) max_speed * 2/3; // on se base sur le moteur gauche
    }

    //init the queue of state
    init_queue(&mqueue_cata_state, O_CREAT | O_WRONLY,"/cataStateQ");

    //init the queue of order
    init_queue (&mqueue_cata, O_RDONLY, "/cataQ");

    //associate signal
    signal(SIGUSR1, sig_stop);
   return ;
}

//le thread qui gere le moteur
void * thread_motor_cata(void *arg)
{
    (void) arg;
    int foo = -1;
    init_motor_cata();
    sleep(1);
    angle_init = motor_get_angle(PORT_MOTOR_CATA);
    printf("angle %d\n", angle_init);
    while (1) {
        printf("ordre : %d\n", foo);
        foo = get_integer_from_mq (mqueue_cata);
        switch(foo) {
            case 0 :
                motor_stop(PORT_MOTOR_CATA, TACHO_HOLD);
                continue;
            case 1 :
                printf("launch close\n");
                launch_close();
                continue;
            case 2 : 
                printf("launch far\n");
                launch_far();
                continue;
            case 3 : 
                printf("get a ball\n");
                get_a_ball();
                continue;
            case 4 :
                makeZero();
                continue;
            case 5:
                reset_arm();
                continue;
            case 6:
                get_a_ball_special();
                continue;
            case 7:
                launch_far_special();
                continue;
            case OVER :
                motor_run(PORT_MOTOR_CATA, 1, 0.1);
                sleep(1);
                motor_stop(PORT_MOTOR_CATA, TACHO_COAST);
                exit(0);
            default :
                continue;
        }
        sleep(5);
    }
}

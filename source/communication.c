#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <sys/socket.h>
#include <math.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/rfcomm.h>
#include "queue.h"
#include <mqueue.h>

#define SERV_ADDR   "14:ab:c5:34:e7:fb"     /* Whatever the address of the server is */
#define TEAM_ID     7                       /* Your team ID */

#define MSG_ACK     0
#define MSG_START   1
#define MSG_STOP    2
#define MSG_KICK    3
#define MSG_SCORE   4
#define MSG_CUSTOM  8
#define OVER        37

#define ADV         15

#define Sleep( msec ) usleep(( msec ) * 1000 )

static int sock;
static mqd_t mqueue_BTin;
static mqd_t mqueue_BTout;

static uint16_t msgId = 0;
static char* msg_predef[2] = {"coucou", "blabla"}; // create a list of emote

static int read_from_server (int sock, char *buffer, size_t maxSize) {
  int bytes_read = read (sock, buffer, maxSize);

  if (bytes_read <= 0) {
    fprintf (stderr, "Server unexpectedly closed connection...\n");
    close (sock);
    exit (EXIT_FAILURE);
  }

  printf ("[DEBUG] received %d bytes\n", bytes_read);

  return bytes_read;
}

void send_score_msg(int score) {
    char msg[5] = {0};
    *((uint16_t *) msg) = msgId++;
    msg[2] = TEAM_ID;
    msg[3] = 0xFF;
    msg[4] = MSG_SCORE;
    msg[5] = score;

    write (sock, msg, 6);
}

void send_ack_msg(char * msg_rec, int state) {
    char msg[5] = {0};
    *((uint16_t *) msg) = msgId++;
    msg[2] = TEAM_ID;
    msg[3] = msg_rec[2];
    msg[4] = MSG_ACK;
    msg[5] = msg_rec[0];
    msg[6] = msg_rec[1];
    msg[7] = state;
    write (sock, msg, 8);
}

void send_custom_msg(int msg_to_send, int dest) {
    char msg[58] = {0};
    *((uint16_t *) msg) = msgId++;
    msg[2] = TEAM_ID;
    msg[3] = dest;
    msg[4] = MSG_CUSTOM;
    strcpy((msg + 5), msg_predef[msg_to_send]); // voir si ca marche ici
    write (sock, msg, 58);
}

int init_socket() {
    struct sockaddr_rc addr = { 0 };
    int status = 5;

    /* allocate a socket */
    sock = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

    /* set the connection parameters (who to connect to) */
    addr.rc_family = AF_BLUETOOTH;
    addr.rc_channel = (uint8_t) 1;
    str2ba (SERV_ADDR, &addr.rc_bdaddr);
    printf("before connecting\n");
    /* connect to server */
    status = connect(sock, (struct sockaddr *)&addr, sizeof(addr));
    sleep(3);
    /* if not connected */
    if( status ) {
        close(sock);
        perror("Failed to connect to server...\n");
    }
    printf("status : connected %d\n", status);
    init_queue (&mqueue_BTin, O_CREAT | O_WRONLY, "/btInQ");
    init_queue (&mqueue_BTout, O_RDONLY, "/btOutQ");
  
    return 0;
}

void * thread_BTin(void *arg) {
    (void) arg;
    while(1) {
        char msg[58];
        read_from_server (sock, msg, 58);
        switch(msg[4]) {
        case MSG_START  :
            put_integer_in_mq(mqueue_BTin, MSG_START);
            break;
        case MSG_STOP   :
            put_integer_in_mq(mqueue_BTin, MSG_STOP);
            exit(0);
            break;
        case OVER :
            exit(0);
        case MSG_KICK   :
            if (msg[5] == TEAM_ID) {
                put_integer_in_mq(mqueue_BTin, MSG_STOP);
                exit(0);
            }
        case MSG_ACK    :
        case MSG_CUSTOM :
        default :
            break ; //do nothing
        }
    }
}

void * thread_BTout(void *arg) {
    (void) arg;
    int data;
    while(1) {
        data = get_integer_from_mq(mqueue_BTout);
        printf("out : %d", data);
        switch((int) data/10) {
            case 4 :
                send_score_msg (data % 10);
                continue;
            case 8 :
                send_custom_msg(ADV, data % 10);
                continue;
        }
    }
}

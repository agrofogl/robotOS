### HOW TO :
#### Clone the repository

```
$git clone git@gitlab.eurecom.fr:agrofogl/robotOS.git
$cd robotOS/
$git submodule init
$git submodule update
```

#### Connect with the robot
We will use a ssh connection with dsa key
`$ssh robot@255.255.255.255`

#### Transfert file on the robot
`$scp $FILE robot@255.255.255.255:DIR`

but you can also use the small script "run" to compile, transfert and run the file on the robot

#### Miscellaneous

* be sure to have the right IP address written on the script
* every file that has been transfert from the computer to the ev3 brick, using scp must be put in `~/Project`
